function [LFRef,LFMask] = complete(LFRef,varargin)
%COMPLETE Summary of this function goes here
%   Detailed explanation goes here

LFSize = size(LFRef);
numDims = ndims(LFRef);
colDims = [];
rowDims = [];

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', colDims, @isnumeric);
p.addParameter('rowDims', rowDims, @isnumeric);

p.parse(varargin{:});
colDims = p.Results.colDims;
rowDims = p.Results.rowDims;

if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = LFSize(colDims);
rowSize = LFSize(rowDims);

LFRef = permute(LFRef,[colDims,rowDims]);
LFRef = reshape(LFRef,prod(colSize),prod(rowSize));

[LFRef,LFMask] = utils.complete(LFRef,varargin{:});

LFRef  = reshape(LFRef  ,[colSize,rowSize]);
LFMask = reshape(LFMask ,[colSize,rowSize]);

LFRef  = ipermute(LFRef ,[colDims,rowDims]);
LFMask = ipermute(LFMask,[colDims,rowDims]);

end