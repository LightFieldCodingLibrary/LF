function [LFRec,nbBits,peaksnr] = codec(LFRef,varargin)
%CODEC Encode/Decode lightfield data
%   [LFRec,nbBits,peaksnr] = codec(LFRef,varargin)

refFilename = fullfile(pwd,'sequence_ref');
recFilename = fullfile(pwd,'sequence_rec');

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('refFilename', refFilename, @ischar);
p.addParameter('recFilename', recFilename, @ischar);
p.addParameter('padding'    , 8          , @isnumeric);

% Parse arguments
p.parse(varargin{:});

refFilename = p.Results.refFilename;
recFilename = p.Results.recFilename;
padding     = p.Results.padding;

Others = p.Unmatched;

% Make sure folders exist
refDir = fileparts(refFilename);
if ~exist(refDir,'dir')
    mkdir(refDir);
end

recDir = fileparts(recFilename);
if ~exist(recDir,'dir')
    mkdir(recDir);
end

% Prepare padding
LFSize  = size(LFRef);
[padSize,imgSizePad,imgResPad] = computePadding(LFSize,padding);

% Compute and separate LF and HEVC parameters
[~,bitDepth] = utils.precision(class(LFRef));
[LFParams,HEVCParams] = LF.LFtoHEVC(refFilename,recFilename,imgSizePad,imgResPad,...
    'bitDepth',bitDepth,Others);

if HEVCParams.encode
    % Do padding
    LFRef = padarray(LFRef,padSize,'replicate','post');
    
    % Write input yuv file
    LFParams.bitDepth = str2double(HEVCParams.InputBitDepth);
    LF.write(LFRef,'filename',HEVCParams.InputFile,LFParams,'addInfo',false);
end

% Encode frames using HEVC
[nbBits,peaksnr] = HEVC.codec(HEVCParams);

if HEVCParams.decode
    % Read output yuv file
    LFParams.bitDepth = str2double(HEVCParams.OutputBitDepth);
    LFRec = LF.read('filename',HEVCParams.ReconFile,LFParams);
    
    % Undo padding
    gv = arrayfun(@(x) 1:x,LFSize,'UniformOutput',false);
    LFRec = LFRec(gv{:});
end

end

function [padSize,imgSizePad,imgResPad] = computePadding(LFSize,padding)
    padSize = ceil(LFSize./padding).*padding-LFSize;
    padSize(3:end) = 0;
    LFSizePad  = LFSize + padSize;
    imgSizePad = LFSizePad(1:3);
    imgResPad  = LFSizePad(4:end);
end