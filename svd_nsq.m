function [U,S,V] = svd_nsq(Z)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               SVD accelerated for non-square matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[m, n] = size(Z);

if 2*m < n
    ZZT = Z*Z';
    [U, S2, Ua] = svd(ZZT);
    S = sqrt(S2);
    Sinv = diag(1./diag(S));
    V = Z' * U * Sinv;
    return;
end
if m > 2*n
    ZTZ = Z'*Z;
    [Va, S2, V] = svd(ZTZ);
    S = sqrt(S2);
    Sinv = diag(1./diag(S));
    U = Z * V * Sinv;
    return;
end

if(m==n)
    [U,S,V] = svd(Z);
else
    [U,S,V] = svd(Z,'econ');
end