function [...
    psnr_y_view  ,psnr_y_mean  ,psnr_y_all  ,ssim_y_view  ,ssim_y_mean  ,...
    psnr_yuv_view,psnr_yuv_mean,psnr_yuv_all,ssim_yuv_view,ssim_yuv_mean,...
    psnr_u_view  ,psnr_u_mean  ,psnr_u_all  ,ssim_u_view  ,ssim_u_mean  ,...
    psnr_v_view  ,psnr_v_mean  ,psnr_v_all  ,ssim_v_view  ,ssim_v_mean  ...
    ] = qualityMetrics(LFRec,LFRef,varargin)
%QUALITYMETRICS Summary of this function goes here
%   Detailed explanation goes here

LFRecSlices = LF.toSlices(LFRec);
LFRefSlices = LF.toSlices(LFRef);

[psnr_y_view,psnr_yuv_view,psnr_u_view,psnr_v_view,...
    ssim_y_view,ssim_yuv_view,ssim_u_view,ssim_v_view] = ...
    cellfun(@(rec,ref) utils.qualityMetrics(rec,ref,varargin{:}),LFRecSlices,LFRefSlices);

psnr_y_view = psnr_y_view(:)';
psnr_yuv_view = psnr_yuv_view(:)';
psnr_u_view = psnr_u_view(:)';
psnr_v_view = psnr_v_view(:)';

ssim_y_view = ssim_y_view(:)';
ssim_yuv_view = ssim_yuv_view(:)';
ssim_u_view = ssim_u_view(:)';
ssim_v_view = ssim_v_view(:)';

psnr_y_mean   = mean(psnr_y_view);
psnr_yuv_mean = mean(psnr_yuv_view);
psnr_u_mean   = mean(psnr_u_view);
psnr_v_mean   = mean(psnr_v_view);

ssim_y_mean   = mean(ssim_y_view);
ssim_yuv_mean = mean(ssim_yuv_view);
ssim_u_mean   = mean(ssim_u_view);
ssim_v_mean   = mean(ssim_v_view);

LFRecSlices = cell2mat(LFRecSlices);
LFRefSlices = cell2mat(LFRefSlices);

[psnr_y_all,psnr_yuv_all,psnr_u_all,psnr_v_all] = ...
    utils.qualityMetrics(LFRecSlices,LFRefSlices,varargin{:});

end