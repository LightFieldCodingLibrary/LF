function [LFB,C,U,S,V] = factorize(LFRef,k,varargin)
%FACTORIZE Summary of this function goes here
%   Detailed explanation goes here

LFSize = size(LFRef);
numDims = ndims(LFRef);
colDims = [];
rowDims = [];

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', colDims, @isnumeric);
p.addParameter('rowDims', rowDims, @isnumeric);

p.parse(varargin{:});
colDims = p.Results.colDims;
rowDims = p.Results.rowDims;

if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = LFSize(colDims);
rowSize = LFSize(rowDims);

LFRef = permute(LFRef,[colDims,rowDims]);
LFRef = reshape(LFRef,prod(colSize),prod(rowSize));

rowSize = [k,1];

[LFB,C,U,S,V] = utils.factorize(LFRef,k);

LFB = reshape (LFB,[colSize,rowSize]);
LFB = ipermute(LFB,[colDims,rowDims]);

end