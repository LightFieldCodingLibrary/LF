function LFRef = replicate(LFRef,ResOff)
%REPLICATE Summary of this function goes here
%   Detailed explanation goes here

LFSize = size(LFRef);
ImgSize = LFSize(1:3);
ImgRes  = LFSize(4:end);
ResOff = num2cell(ResOff);

LFRef  = reshape(LFRef,prod(ImgSize),prod(ImgRes));
LFRef = utils.replicate(LFRef,sub2ind(ImgRes,ResOff{:}));
LFRef  = reshape(LFRef ,[ImgSize,ImgRes]);

end