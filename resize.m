function Val = resize(Val,split,varargin)
%RESIZE Summary of this function goes here
%   Detailed explanation goes here

assert(ndims(Val)==length(split))
if isequal(split,ones(1,ndims(Val)))
    return
end

%% Parse input
interps = {'nearest','linear','cubic','spline'};
validFcn = @(x) any(validatestring(x,interps));

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addOptional('interpMethod', 'nearest', validFcn);
p.parse(varargin{:});
interpMethod = p.Results.interpMethod;

p.addOptional('extrapMethod', interpMethod, validFcn);
p.parse(p.Unmatched);
extrapMethod = p.Results.extrapMethod;

%% Set interpolant
LFSize = size(Val);
gv = arrayfun(@(split,sz) 1:split:sz,split,LFSize,'UniformOutput',false);

LFValInt = griddedInterpolant(Val,interpMethod,extrapMethod);

%% Interpolate
Val = LFValInt(gv);
end